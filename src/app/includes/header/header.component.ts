import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  dellAvailable(){
    return false;
  }
  hpAvailable(){
    return false;
  }

  
  

  ngOnInit(): void {
    let buyLaptop= new Promise((resolve,reject)=>{
     if (this.dellAvailable()){
       return setTimeout(()=>{
         resolve('Dell is Purchased');
       }, 3000)
     }
     else if(this.hpAvailable()){
       return setTimeout(()=>{
         resolve('Hp is Purchased');
       }, 3000);
     }
     else{
       return setTimeout(()=>{
         reject('Laptop is not available on the store');
       },3000);
     }
    });
  buyLaptop.then(res=> {
    console.log("Then Code =>",res);
  }).catch(res =>{
    console.log("Now The Code is =>",res);
  })
  }
  

}
