import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DesignUtilityService } from 'src/app/appServices/design-utility.service';

@Component({
  selector: 'app-custom',
  templateUrl: './custom.component.html',
  styleUrls: ['./custom.component.scss']
})
export class CustomComponent implements OnInit {
  
  constructor(private _designUtility:DesignUtilityService) { }
 
  ngOnInit(): void {
  
    const cusObs1= Observable.create((observer:any) =>{
     setTimeout(()=>{
      observer.next('Angular');
     },1000);
     setTimeout(()=>{
      observer.next('Html');
     },3000);
     setTimeout(()=>{
      observer.next('CSS');
     },4000);

     setTimeout(()=>{
      observer.next('Javascript');
     },5000);
     setTimeout(()=>{
      observer.next('React');
     },6000);
     setTimeout(()=>{
      observer.next('Bootstrap');
     },7000);
     setTimeout(()=>{
      observer.next('Node');
     },8000);

    })
    cusObs1.subscribe((res:any)=>{
      console.log(res);
      this._designUtility.print(res,'elContainer');
    }) 
  }

}
