import { Component, OnInit } from '@angular/core';
import { from, of } from 'rxjs';
import { DesignUtilityService } from 'src/app/appServices/design-utility.service';

@Component({
  selector: 'app-of-from',
  templateUrl: './of-from.component.html',
  styleUrls: ['./of-from.component.scss']
})
export class OfFromComponent implements OnInit {
  obsMsg!: any;

  constructor(private _designUtility:DesignUtilityService) { }

  ngOnInit(): void {
    const Obs1= of('Ajay','Shankar','Sharma');
    Obs1.subscribe(res=>{
      console.log(res);
      this._designUtility.print(res,'elContainer');
    })
    const Obs2= of({a:'Ajay', b:'Shankar', c:'Sharma'});
    Obs2.subscribe(res=>{
      this.obsMsg=res;
      console.log('obsMsg=>', res);
      
    })
    const Obs3= from(['Ajay','Shankar','Sharma']);
    Obs3.subscribe(res=>{
      console.log('obsMsg=>', res);
      this._designUtility.print(res,'elContainer3');
    })
    const promise= new Promise(resolve =>{
      setTimeout(()=>{
        resolve ('Promise Resolved ');

      },3000);
    })
    const obs4=from(promise);
    obs4.subscribe(res=>{
      console.log(res);
      this._designUtility.print(res, 'elContainer4');
    })
    const obs5=from('Welcome to the Jungle');
    obs5.subscribe(res=>{
      console.log(res);
      this._designUtility.print(res, 'elContainer5');
    })
  }

}
