import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { filter, toArray } from 'rxjs/operators';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  constructor() { }
  datArr=[
    {id: 1, name:'Anup', gender:'Male'},
    {id: 2, name: 'Priyanka', gender:'Female'},
    {id: 3, name:'Ashish', gender:'Male'},
    {id: 4, name:'Vivek', gender:'Male'},
    {id: 5, name:'Jannat', gender:'Female'},
    {id: 6, name:'Monika', gender:'Female'},
    {id: 7, name:'Rajesh', gender:'Male'},
    {id: 8, name:'Sanjana', gender:'Female'},
    {id: 9, name:'Neha', gender:'Female'},
    {id: 10, name:'Sakhshi', gender:'Female'},
    {id: 11, name:'Neeraj', gender:'Male'},
    {id: 12, name:'Pradeep', gender:'Male'}
  ]
  data:any;
  data2:any;
  data3:any;

  ngOnInit(): void {
    const source = from(this.datArr)
    source.pipe(
      filter(member => member.name.length <= 6),
      toArray()
    )
    .subscribe(res =>{
      console.log(res)
      this.data=res
    })


    source.pipe(
      filter(member => member.id<=6 ),
      toArray()
    )
    .subscribe(res =>{
      console.log(res)
      this.data2=res
    })


    source.pipe(
      filter(member => member.gender == "Male"),
      toArray()
    )
    .subscribe(res =>{
      console.log(res)
      this.data3=res
    })
  }

}
