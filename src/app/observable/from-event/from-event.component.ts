import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { DesignUtilityService } from 'src/app/appServices/design-utility.service';

@Component({
  selector: 'app-from-event',
  templateUrl: './from-event.component.html',
  styleUrls: ['./from-event.component.scss']
})
export class FromEventComponent implements OnInit {

  constructor(private _designUtility: DesignUtilityService) { }
  @ViewChild('addBtn')
  addBtn!: ElementRef; 

  ngOnInit(): void {
  
  }
  ngAfterViewInit(){
    let count=1;
    fromEvent(this.addBtn.nativeElement, 'click').subscribe(res=>{
      let element = 'Video: '+count++;
      console.log(element);
      this._designUtility.print(element , 'elContainer');
      this._designUtility.print(element , 'elContainer2');
    })
  }
 

}
