import { Component, OnInit } from '@angular/core';
import { from, interval } from 'rxjs';
import { take, toArray } from 'rxjs/operators';

@Component({
  selector: 'app-to-array',
  templateUrl: './to-array.component.html',
  styleUrls: ['./to-array.component.scss']
})
export class ToArrayComponent implements OnInit {
  sourceSub: any;
  users=[
    {name:'Anup', skill:'Angular'},
    {name:'Shankar', skill:'HTML,CSS'},
    {name:'Sharma', skill:'JavaScript'},
    {name:'Nitin', skill:'TypeScript'}
  ];

  constructor() { }

  ngOnInit(): void {
    const source=interval(1000);
    this.sourceSub=source.pipe(
      take(5),
      toArray()
    )
    .subscribe(res=>{
      console.log(res);
    })
    const source2=from(this.users);
    
    source2.pipe(toArray())
    .subscribe((res)=>{
      console.log(res);
    })
  }

}
