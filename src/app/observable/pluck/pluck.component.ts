import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { map, pluck, toArray } from 'rxjs/operators';

@Component({
  selector: 'app-pluck',
  templateUrl: './pluck.component.html',
  styleUrls: ['./pluck.component.scss']
})
export class PluckComponent implements OnInit {

  constructor() { }
  data:any;
  users=[
    {
      name:"Nitin",
      skills:"Angular",
      job:{
        title: "UI developer",
        exp: 10 
      }
    },
    {
      name:"Vibhore",
      skills:"Angular",
      job:{
        title: "js",
        exp: 10 
      }
    },
    {
      name:"Pushpendra",
      skills:"React",
      job:{
        title: "React",
        exp: 7
      }
    },
    {
      name:"Rishabh",
      skills:"Node",
      job:{
        title: "Java",
        exp: 3
      }
    },
    {
      name:".net",
      skills:"Node",
      job:{
        title: "Python",
        exp: 4 
      }
    }
  ]
  ngOnInit(): void {
    from(this.users).pipe(
      // map( data =>data.name),
      pluck('job','title'),
      toArray()
    )
    .subscribe(res =>{
      console.log(res)
      this.data = res
    })
  }
}
