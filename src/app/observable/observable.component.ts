import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { take, toArray } from 'rxjs/operators';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.scss']
})
export class ObservableComponent implements OnInit {
  sourceSub: any;

  constructor() { }

  ngOnInit(): void {
    const source= interval(100);
    this.sourceSub=source.pipe(
      take(5),
      toArray()
    )
    .subscribe(res=>{
      console.log(res);
    })
  }

}
