import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DesignUtilityService {

  constructor() { }

  print(element:any, containerId:string){
    let el=document.createElement('li');
    el.innerText=element;
    document.getElementById(containerId)?.appendChild(el);
  }
}
